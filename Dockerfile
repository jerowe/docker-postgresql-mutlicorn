FROM fedora
MAINTAINER http://fedoraproject.org/wiki/Cloud
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN yum -y update && yum clean all
RUN yum -y install postgresql postgresql-server postgresql-contrib postgresql-devel supervisor python-pip python-devel  && yum clean all
RUN yum -y install git wget zlib-devel tar bzip2 ncurses-devel && yum clean all
RUN yum -y groupinstall "Development Tools" && yum clean all

ADD . /src

RUN cd /src; pip install -r requirements.txt

RUN pgxn install multicorn

## Samtools and things 

RUN cd && wget https://github.com/samtools/htslib/releases/download/1.2.1/htslib-1.2.1.tar.bz2 && tar -xvf htslib-1.2.1.tar.bz2 && cd htslib-1.2.1 && make && make install
RUN cd && wget https://github.com/samtools/samtools/releases/download/1.2/samtools-1.2.tar.bz2 && tar -xvf samtools-1.2.tar.bz2 && cd samtools-1.2 && make && make install
RUN cd && wget https://github.com/samtools/bcftools/releases/download/1.2/bcftools-1.2.tar.bz2 && tar -xvf bcftools-1.2.tar.bz2 && cd bcftools-1.2 && make && make install

ADD ./supervisord.conf /etc/supervisord.conf

RUN sed -i -e '2,2ialias system=echo' -e '2,2ifunction systemctl() { echo "Environment=PGPORT=5432 PGDATA=/var/lib/pgsql/data"; }' /usr/bin/postgresql-setup && \
( tail -F /var/lib/pgsql/initdb.log & tailpid=$!; \
export $(locale 2>>/dev/null|tr -d '\\"') ; \
/usr/bin/postgresql-setup initdb && \
kill $tailpid )

RUN echo "host all all 0.0.0.0/0 trust" >> /var/lib/pgsql/data/pg_hba.conf
RUN echo "listen_addresses='*'" >> /var/lib/pgsql/data/postgresql.conf

USER postgres
WORKDIR /var/lib/pgsql

EXPOSE 5432

USER root
CMD ["/usr/bin/supervisord", "-n"]

